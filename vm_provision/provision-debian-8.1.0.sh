#!/usr/bin/env bash

# Intended for Debian

# Update 
apt-get update

# Adjust timezone to be Phoenix
ln -sf /usr/share/zoneinfo/America/Phoenix /etc/localtime

#Install git
echo "----- Provision: Installing git..."
sudo apt-get -y install git

#Install postreqsql
echo "----- Provision: Installing postgresql 9.4..."
sudo apt-get -y install postgresql-9.4

# Install perlbrew
echo "----- Provision: Installing perlbrew"
sudo -H -u vagrant bash -c " \curl -kL https://install.perlbrew.pl | bash"
echo "source ~/perl5/perlbrew/etc/bashrc" >> /etc/bash.bashrc

# Install perl 5.25.0
echo "----- Provision: Installing perl-5.24.0..."
sudo -H -u vagrant bash -c "~/perl5/perlbrew/bin/perlbrew install perl-5.24.0 "
sudo -H -u vagrant bash -c "~/perl5/perlbrew/bin/perlbrew switch perl-5.24.0"
sudo -H -u vagrant bash -c "which perl"

# Install cpanminus
echo "----- Provision: Installing cpanminus"
sudo -H -u vagrant bash -c "~/perl5/perlbrew/bin/p erlbrew install-cpanm"
echo -e "\nsource ~/perl5/perlbrew/etc/bashrc" >> /etc/bash.bashrc
sudo -H -u vagrant bash -c "which cpanm"

# Install RapidApp
echo "----- Provision: Installing RapidApp..."
sudo -H -u vagrant bash -c "~/perl5/perlbrew/bin/perlbrew exec --with perl-5.24.0 cpanm --notest RapidApp"

# Cleanup
apt-get -y autoremove

#"~/perl5/perlbrew/bin/cpanm --notest  RapidApp"

