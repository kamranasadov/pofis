use strict;
use warnings;

use pofis;

my $app = pofis->apply_default_middlewares(pofis->psgi_app);
$app;

