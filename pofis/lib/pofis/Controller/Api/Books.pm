package pofis::Controller::Api::Books;
use Moose;
use namespace::autoclean;
use RapidApp::Util qw(:all);

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

pofis::Controller::Books - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

}

sub test :Local {
  my ( $self, $c ) = @_;
}


=encoding utf8

=head1 AUTHOR

Kamran,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
